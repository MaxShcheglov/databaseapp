﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace DatabaseApp
{
    internal class DatabaseLayer
    {
        private readonly string _assemblyPath;
        private SqlConnection _sqlConnection;

        public DatabaseLayer()
        {
            _assemblyPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}";

            using var sqlConnection = new Microsoft.Data.SqlClient.SqlConnection(@"Integrated Security=SSPI; Data Source=(LocalDB)\MSSQLLocalDB");
            var serverConnection = new ServerConnection(sqlConnection);
            var server = new Server(serverConnection);
            ErrorLogPath = server.ErrorLogPath;
        }

        public string ErrorLogPath { get; }

        public bool IsDatabaseExists(string databaseName) => File.Exists(Path.Combine(_assemblyPath, databaseName + ".mdf"));

        public bool IsDatabaseAlreadyOpened(string databaseName) => _sqlConnection.Database.Equals(databaseName);

        public List<string> Databases()
        {
            return Directory.GetFileSystemEntries(_assemblyPath).Where(x => x.Contains(".mdf")).Select(path => new FileInfo(path).Name).ToList();
        }

        public int Open(string databaseName)
        {
            try
            {
                var connectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={_assemblyPath}\{databaseName}.mdf;Integrated Security=True";
                _sqlConnection = new SqlConnection(connectionString);
                _sqlConnection.Open();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 1;
            }

            return 0;
        }

        public int Close()
        {
            try
            {
                _sqlConnection.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 1;
            }

            return 0;
        }

        public int Create(string databaseName)
        {
            try
            {
                var query = $@"CREATE DATABASE {databaseName} ON PRIMARY 
                           (NAME = {databaseName}_Data,
                           FILENAME = '{_assemblyPath}\\{databaseName}.mdf',
                           SIZE = 2MB, MAXSIZE = 10MB, FILEGROWTH = 10%)
                           LOG ON (NAME = {databaseName}_Log,
                           FILENAME = '{_assemblyPath}\\{databaseName}.ldf',
                           SIZE = 1MB, 
                           MAXSIZE = 5MB,
                           FILEGROWTH = 10%)";

                var myCommand = new SqlCommand(query, _sqlConnection);
                myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 1;
            }

            return 0;
        }

        public int Delete(string databaseName)
        {
            try
            {
                var query = $"DROP DATABASE {databaseName}";
                var myCommand = new SqlCommand(query, _sqlConnection);
                myCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 1;
            }

            return 0;
        }

        public int Flush()
        {
            try
            {
                var query = @"SELECT TABLE_NAME
                          FROM INFORMATION_SCHEMA.TABLES";

                var tableNames = new List<string>();

                var myCommand = new SqlCommand(query, _sqlConnection);
                using var reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    tableNames.Add(reader.GetString(0));
                }
                reader.Close();

                foreach (var tableName in tableNames)
                {
                    query = $"DROP TABLE [{tableName}]";
                    myCommand = new SqlCommand(query, _sqlConnection);
                    myCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return 1;
            }

            return 0;
        }

        public void InsertStudent(Student student)
        {
            var query = $@"INSERT INTO Students
                           (FirstName, LastName, Birthday, AverageScore)
                           VALUES
                           ('{student.FirstName}', '{student.LastName}', '{student.Birthday.ToShortDateString()}', {student.AverageScore} )";

            var myCommand = new SqlCommand(query, _sqlConnection);
            myCommand.ExecuteNonQuery();
        }

        public List<Student> GetStudents(string fieldBy = "", string valueBy = "")
        {
            var result = new List<Student>();

            var query = $@"SELECT * FROM Students";
            if (!string.IsNullOrEmpty(fieldBy) && !string.IsNullOrEmpty(valueBy))
                query += $"WHERE {fieldBy} = '{valueBy}'";

            var myCommand = new SqlCommand(query, _sqlConnection);
            var reader = myCommand.ExecuteReader();
            while (reader.Read())
            {
                var student = new Student
                {
                    Id = (int)reader.GetValue(0),
                    FirstName = reader.GetValue(1) as string,
                    LastName = reader.GetValue(2) as string,
                    Birthday = DateTime.Parse(reader.GetValue(3).ToString()),
                    AverageScore = double.Parse(reader.GetValue(4).ToString())
                };
                result.Add(student);
            }
            reader.Close();

            return result;
        }

        public Student GetStudent(Student student)
        {
            Student result = null;

            var query = $@"SELECT * FROM Students WHERE Id = {student.Id}";
            var myCommand = new SqlCommand(query, _sqlConnection);
            var reader = myCommand.ExecuteReader();
            if (reader.Read())
            {
                result = new Student
                {
                    Id = (int) reader.GetValue(0),
                    FirstName = reader.GetValue(1) as string,
                    LastName = reader.GetValue(2) as string,
                    Birthday = DateTime.Parse(reader.GetValue(3).ToString()),
                    AverageScore = double.Parse(reader.GetValue(4).ToString())
                };
            }
            reader.Close();

            return result;
        }

        public void RemoveStudent(Student student, string fieldBy = "Id")
        {
            var targetValue = student.GetType().GetProperty(fieldBy)?.GetValue(student, null);

            var query = $@"DELETE FROM Students WHERE {fieldBy} = '{targetValue}'";

            var myCommand = new SqlCommand(query, _sqlConnection);
            myCommand.ExecuteNonQuery();
        }

        public void UpdateStudent(Student student, string fieldBy = "Id")
        {
            var targetValue = student.GetType().GetProperty(fieldBy)?.GetValue(student, null);

            var query = $@"UPDATE Students SET
                           FirstName = '{student.FirstName}',
                           LastName = '{student.LastName}',
                           Birthday = '{student.Birthday.ToShortDateString()}',
                           AverageScore = {student.AverageScore}
                           WHERE {fieldBy} = {targetValue}";

            var myCommand = new SqlCommand(query, _sqlConnection);
            myCommand.ExecuteNonQuery();
        }

        public Microsoft.Data.SqlClient.SqlDataAdapter GetStudentsDataAdapter(string query) => 
            new Microsoft.Data.SqlClient.SqlDataAdapter(
                new Microsoft.Data.SqlClient.SqlCommand(query, 
                    new Microsoft.Data.SqlClient.SqlConnection(_sqlConnection.ConnectionString)));

        public void CreateBackup(string databaseName, string backupFileName)
        {
            var backupDatabaseFull = new Backup {Action = BackupActionType.Database, Database = @$"{_assemblyPath}\{databaseName}.mdf" };

            backupDatabaseFull.Devices.AddDevice(@$"{_assemblyPath}\{backupFileName}.bak", DeviceType.File);
            backupDatabaseFull.BackupSetName = "Students database Backup";
            backupDatabaseFull.BackupSetDescription = "Students database - Full Backup";
            backupDatabaseFull.ExpirationDate = DateTime.Today.AddDays(10);
            backupDatabaseFull.Initialize = false;

            using var sqlConnection = new Microsoft.Data.SqlClient.SqlConnection(@"Integrated Security=SSPI; Data Source=(LocalDB)\MSSQLLocalDB");
            var serverConnection = new ServerConnection(sqlConnection);
            var server = new Server(serverConnection);

            backupDatabaseFull.SqlBackup(server);
        }

        public void RestoreFromBackup(string databaseName, string backupFileName)
        {
            // TODO not working for now, cant get exclusive access
            // RESTORE DATABASE [C:\Users\Maximus\source\repos\DatabaseApp\DatabaseApp\bin\Debug\Database1.mdf] FROM  DISK = N'C:\Users\Maximus\source\repos\DatabaseApp\DatabaseApp\bin\Debug\backup.bak' WITH  NOUNLOAD,  REPLACE,  STATS = 10
            // ALTER DATABASE [MyDB] SET Single_User WITH Rollback Immediate
            // ALTER DATABASE [MyDB] SET Multi_User

            using var sqlConnection = new Microsoft.Data.SqlClient.SqlConnection(@"Integrated Security=SSPI; Data Source=(LocalDB)\MSSQLLocalDB");
            var serverConnection = new ServerConnection(sqlConnection);
            var server = new Server(serverConnection);

            var restoreDatabase = new Restore();
            restoreDatabase.Action = RestoreActionType.Database;
            restoreDatabase.Database = @$"{_assemblyPath}\{databaseName}.mdf";

            var backupDeviceItem = new BackupDeviceItem(@$"{_assemblyPath}\{backupFileName}.bak", DeviceType.File);
            restoreDatabase.Devices.Add(backupDeviceItem);
            restoreDatabase.ReplaceDatabase = true;
            restoreDatabase.SqlRestore(server);
        }
    }

    internal class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public double AverageScore { get; set; }
    }
}