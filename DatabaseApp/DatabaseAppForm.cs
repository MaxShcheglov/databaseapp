﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DatabaseApp
{
    public partial class DatabaseAppForm : Form
    {
        private readonly DatabaseLayer _databaseLayer;
        private readonly BindingSource _studentsBindingSource = new BindingSource();
        private List<string> _logs = new List<string>();

        public DatabaseAppForm()
        {
            InitializeComponent();

            _databaseLayer = new DatabaseLayer();

            _databaseLayer.Open("Database1");

            UpdateLogs();
            UpdateDatabasesList();
            RefreshStudentsTable();
            InitializeControls();
        }

        private void InitializeControls()
        {
            comboBoxOperation.SelectedIndex = 0;
            comboBoxField.SelectedIndex = 1;
        }

        private void UpdateLogs()
        {
            labelLogs.Text = GetLastLogLines(20);
        }

        private void UpdateDatabasesList()
        {
            var databaseNames = _databaseLayer.Databases();

            var stringBuilder = new StringBuilder();
            foreach (var databaseName in databaseNames)
                stringBuilder.AppendLine(databaseName);

            labelDatabases.Text = stringBuilder.ToString();
        }

        private string GetLastLogLines(int linesNumber)
        {
            _logs.Reverse();
            _logs = _logs.Take(linesNumber).ToList();
            _logs.Reverse();

            var stringBuilder = new StringBuilder();
            foreach (var log in _logs)
                stringBuilder.AppendLine(log);

            return stringBuilder.ToString();
        }

        private void buttonOpenDatabase_Click(object sender, EventArgs e)
        {
            var result = _databaseLayer.Open(textBoxDatabaseName.Text);

            _logs.Add(result != 0
                ? $"Can not open database {textBoxDatabaseName.Text}"
                : $"Database {textBoxDatabaseName.Text} successfully opened");

            UpdateLogs();
            UpdateDatabasesList();
        }

        private void buttonCloseDatabase_Click(object sender, EventArgs e)
        {
            var result = _databaseLayer.Close();

            _logs.Add(result != 0
                ? $"Can not close database {textBoxDatabaseName.Text}"
                : $"Database {textBoxDatabaseName.Text} successfully closed");

            UpdateLogs();
            UpdateDatabasesList();
        }

        private void buttonCreateDatabase_Click(object sender, EventArgs e)
        {
            var result = _databaseLayer.Create(textBoxDatabaseName.Text);

            _logs.Add(result != 0
                ? $"Can not create database {textBoxDatabaseName.Text}"
                : $"Database {textBoxDatabaseName.Text} successfully created");

            UpdateLogs();
            UpdateDatabasesList();
        }

        private void buttonFlushDatabase_Click(object sender, EventArgs e)
        {
            var result = _databaseLayer.Flush();

            _logs.Add(result != 0
                ? $"Can not flush database {textBoxDatabaseName.Text}"
                : $"Database {textBoxDatabaseName.Text} successfully flushed");

            UpdateLogs();
            UpdateDatabasesList();
        }

        private void buttonDeleteDatabase_Click(object sender, EventArgs e)
        {
            var result = _databaseLayer.Delete(textBoxDatabaseName.Text);

            _logs.Add(result != 0
                ? $"Can not delete database {textBoxDatabaseName.Text}"
                : $"Database {textBoxDatabaseName.Text} successfully deleted");

            UpdateLogs();
            UpdateDatabasesList();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            InitializeStudent(out var student);
            Patch(student, comboBoxField.Text, textBoxValue.Text);
            PerformOperation(student, comboBoxOperation.Text, comboBoxField.Text);

            RefreshStudentsTable();
        }

        private void InitializeStudent(out Student student)
        {
            student = new Student
            {
                FirstName = "Mike",
                LastName = "Shinoda",
                Birthday = DateTime.Now.AddYears(-35),
                AverageScore = 5.0
            };

            if (comboBoxOperation.Text.Equals("update"))
            {
                student = new Student
                {
                    FirstName = textBoxFirstName.Text,
                    LastName = textBoxLastName.Text,
                    Birthday = DateTime.Parse(textBoxBirthday.Text),
                    AverageScore = double.Parse(textBoxAverageScore.Text)
                };
            }
        }

        private void Patch(Student student, string field, string value)
        {
            switch (field)
            {
                case nameof(student.Id):
                    student.Id = int.Parse(value);
                    break;
                case nameof(student.FirstName):
                    student.FirstName = value;
                    break;
                case nameof(student.LastName):
                    student.LastName = value;
                    break;
                case nameof(student.Birthday):
                    student.Birthday = DateTime.Parse(value);
                    break;
                case nameof(student.AverageScore):
                    student.AverageScore = double.Parse(value);
                    break;
            }
        }

        private void PerformOperation(Student student, string operation, string field)
        {
            switch (operation)
            {
                case "insert":

                    var isStudentAlreadyExists = _databaseLayer.GetStudent(student) != null;

                    // check by uniq id
                    if (!isStudentAlreadyExists)
                        _databaseLayer.InsertStudent(student);

                    break;
                case "select":

                    // do nothing

                    break;
                case "update":

                    _databaseLayer.UpdateStudent(student);

                    break;
                case "remove":

                    _databaseLayer.RemoveStudent(student, field);

                    break;
            }
        }

        private void RefreshStudentsTable()
        {
            var query = "SELECT * FROM Students";

            if (comboBoxOperation.Text.Equals("select"))
                query += $" WHERE {comboBoxField.Text} = '{textBoxValue.Text}'";

            var table = new DataTable {Locale = System.Globalization.CultureInfo.InvariantCulture};
            _databaseLayer.GetStudentsDataAdapter(query).Fill(table);

            _studentsBindingSource.DataSource = table;

            studentsGridView.DataSource = _studentsBindingSource;
        }

        private void comboBoxOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!comboBoxOperation.Text.Equals("update"))
                return;

            comboBoxField.SelectedIndex = 0;
        }

        private void comboBoxField_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxOperation.Text.Equals("update") && !comboBoxField.Text.Equals(nameof(Student.Id)))
                comboBoxField.SelectedIndex = 0;

            if (!comboBoxField.Text.Equals(nameof(Student.Id)))
                return;

            var isInteger = int.TryParse(textBoxValue.Text, out _);
            if (!isInteger)
                textBoxValue.Text = 1.ToString();
        }

        private void textBoxValue_TextChanged(object sender, EventArgs e)
        {
            var isInteger = int.TryParse(textBoxValue.Text, out _);

            if (comboBoxOperation.Text.Equals("update") && comboBoxField.Text.Equals(nameof(Student.Id)) && isInteger)
            {
                var selectedStudent = _databaseLayer.GetStudent(new Student { Id = int.Parse(textBoxValue.Text) });
                if (selectedStudent != null)
                {
                    textBoxId.Text = selectedStudent.Id.ToString();
                    textBoxFirstName.Text = selectedStudent.FirstName;
                    textBoxLastName.Text = selectedStudent.LastName;
                    textBoxBirthday.Text = selectedStudent.Birthday.ToShortDateString();
                    textBoxAverageScore.Text = selectedStudent.AverageScore.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        private void buttonBackup_Click(object sender, EventArgs e)
        {
            _databaseLayer.CreateBackup("Database1", "backup");
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            _databaseLayer.RestoreFromBackup("Database1", "backup");
            RefreshStudentsTable();
        }
    }
}
