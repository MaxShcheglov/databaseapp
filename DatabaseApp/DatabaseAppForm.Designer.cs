﻿namespace DatabaseApp
{
    partial class DatabaseAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDatabases = new System.Windows.Forms.Label();
            this.labelLogs = new System.Windows.Forms.Label();
            this.textBoxDatabaseName = new System.Windows.Forms.TextBox();
            this.buttonCloseDatabase = new System.Windows.Forms.Button();
            this.buttonFlushDatabase = new System.Windows.Forms.Button();
            this.buttonOpenDatabase = new System.Windows.Forms.Button();
            this.buttonDeleteDatabase = new System.Windows.Forms.Button();
            this.buttonCreateDatabase = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxAverageScore = new System.Windows.Forms.TextBox();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.comboBoxField = new System.Windows.Forms.ComboBox();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.comboBoxOperation = new System.Windows.Forms.ComboBox();
            this.studentsGridView = new System.Windows.Forms.DataGridView();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonBackup = new System.Windows.Forms.Button();
            this.buttonRestore = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(28, 23);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(743, 399);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.labelDatabases);
            this.tabPage1.Controls.Add(this.labelLogs);
            this.tabPage1.Controls.Add(this.textBoxDatabaseName);
            this.tabPage1.Controls.Add(this.buttonCloseDatabase);
            this.tabPage1.Controls.Add(this.buttonFlushDatabase);
            this.tabPage1.Controls.Add(this.buttonOpenDatabase);
            this.tabPage1.Controls.Add(this.buttonDeleteDatabase);
            this.tabPage1.Controls.Add(this.buttonCreateDatabase);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(735, 373);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Database";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(555, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 23);
            this.label2.TabIndex = 8;
            this.label2.Text = "Databases";
            // 
            // labelDatabases
            // 
            this.labelDatabases.Location = new System.Drawing.Point(555, 94);
            this.labelDatabases.Name = "labelDatabases";
            this.labelDatabases.Size = new System.Drawing.Size(174, 276);
            this.labelDatabases.TabIndex = 7;
            this.labelDatabases.Text = "databases";
            // 
            // labelLogs
            // 
            this.labelLogs.Location = new System.Drawing.Point(6, 63);
            this.labelLogs.Name = "labelLogs";
            this.labelLogs.Size = new System.Drawing.Size(543, 307);
            this.labelLogs.TabIndex = 6;
            this.labelLogs.Text = "Logs";
            // 
            // textBoxDatabaseName
            // 
            this.textBoxDatabaseName.Location = new System.Drawing.Point(9, 24);
            this.textBoxDatabaseName.Name = "textBoxDatabaseName";
            this.textBoxDatabaseName.Size = new System.Drawing.Size(135, 20);
            this.textBoxDatabaseName.TabIndex = 5;
            // 
            // buttonCloseDatabase
            // 
            this.buttonCloseDatabase.Location = new System.Drawing.Point(312, 22);
            this.buttonCloseDatabase.Name = "buttonCloseDatabase";
            this.buttonCloseDatabase.Size = new System.Drawing.Size(75, 23);
            this.buttonCloseDatabase.TabIndex = 4;
            this.buttonCloseDatabase.Text = "Close";
            this.buttonCloseDatabase.UseVisualStyleBackColor = true;
            this.buttonCloseDatabase.Click += new System.EventHandler(this.buttonCloseDatabase_Click);
            // 
            // buttonFlushDatabase
            // 
            this.buttonFlushDatabase.Location = new System.Drawing.Point(393, 22);
            this.buttonFlushDatabase.Name = "buttonFlushDatabase";
            this.buttonFlushDatabase.Size = new System.Drawing.Size(75, 23);
            this.buttonFlushDatabase.TabIndex = 3;
            this.buttonFlushDatabase.Text = "Flush";
            this.buttonFlushDatabase.UseVisualStyleBackColor = true;
            this.buttonFlushDatabase.Click += new System.EventHandler(this.buttonFlushDatabase_Click);
            // 
            // buttonOpenDatabase
            // 
            this.buttonOpenDatabase.Location = new System.Drawing.Point(231, 22);
            this.buttonOpenDatabase.Name = "buttonOpenDatabase";
            this.buttonOpenDatabase.Size = new System.Drawing.Size(75, 23);
            this.buttonOpenDatabase.TabIndex = 2;
            this.buttonOpenDatabase.Text = "Open";
            this.buttonOpenDatabase.UseVisualStyleBackColor = true;
            this.buttonOpenDatabase.Click += new System.EventHandler(this.buttonOpenDatabase_Click);
            // 
            // buttonDeleteDatabase
            // 
            this.buttonDeleteDatabase.Location = new System.Drawing.Point(474, 22);
            this.buttonDeleteDatabase.Name = "buttonDeleteDatabase";
            this.buttonDeleteDatabase.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteDatabase.TabIndex = 1;
            this.buttonDeleteDatabase.Text = "Delete";
            this.buttonDeleteDatabase.UseVisualStyleBackColor = true;
            this.buttonDeleteDatabase.Click += new System.EventHandler(this.buttonDeleteDatabase_Click);
            // 
            // buttonCreateDatabase
            // 
            this.buttonCreateDatabase.Location = new System.Drawing.Point(150, 22);
            this.buttonCreateDatabase.Name = "buttonCreateDatabase";
            this.buttonCreateDatabase.Size = new System.Drawing.Size(75, 23);
            this.buttonCreateDatabase.TabIndex = 0;
            this.buttonCreateDatabase.Text = "Create";
            this.buttonCreateDatabase.UseVisualStyleBackColor = true;
            this.buttonCreateDatabase.Click += new System.EventHandler(this.buttonCreateDatabase_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonRestore);
            this.tabPage2.Controls.Add(this.buttonBackup);
            this.tabPage2.Controls.Add(this.textBoxAverageScore);
            this.tabPage2.Controls.Add(this.textBoxBirthday);
            this.tabPage2.Controls.Add(this.textBoxLastName);
            this.tabPage2.Controls.Add(this.textBoxFirstName);
            this.tabPage2.Controls.Add(this.textBoxId);
            this.tabPage2.Controls.Add(this.comboBoxField);
            this.tabPage2.Controls.Add(this.textBoxValue);
            this.tabPage2.Controls.Add(this.comboBoxOperation);
            this.tabPage2.Controls.Add(this.studentsGridView);
            this.tabPage2.Controls.Add(this.buttonApply);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(735, 373);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Table";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBoxAverageScore
            // 
            this.textBoxAverageScore.Location = new System.Drawing.Point(584, 220);
            this.textBoxAverageScore.Name = "textBoxAverageScore";
            this.textBoxAverageScore.Size = new System.Drawing.Size(127, 20);
            this.textBoxAverageScore.TabIndex = 10;
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.Location = new System.Drawing.Point(584, 194);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(127, 20);
            this.textBoxBirthday.TabIndex = 9;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(584, 168);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(127, 20);
            this.textBoxLastName.TabIndex = 8;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(584, 142);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(127, 20);
            this.textBoxFirstName.TabIndex = 7;
            // 
            // textBoxId
            // 
            this.textBoxId.Enabled = false;
            this.textBoxId.Location = new System.Drawing.Point(584, 116);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(127, 20);
            this.textBoxId.TabIndex = 6;
            // 
            // comboBoxField
            // 
            this.comboBoxField.FormattingEnabled = true;
            this.comboBoxField.Items.AddRange(new object[] {
            "Id",
            "FirstName",
            "LastName",
            "Birthday",
            "AverageScore"});
            this.comboBoxField.Location = new System.Drawing.Point(148, 23);
            this.comboBoxField.Name = "comboBoxField";
            this.comboBoxField.Size = new System.Drawing.Size(121, 21);
            this.comboBoxField.TabIndex = 5;
            this.comboBoxField.SelectedIndexChanged += new System.EventHandler(this.comboBoxField_SelectedIndexChanged);
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(275, 23);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxValue.TabIndex = 4;
            this.textBoxValue.Text = "Jeff";
            this.textBoxValue.TextChanged += new System.EventHandler(this.textBoxValue_TextChanged);
            // 
            // comboBoxOperation
            // 
            this.comboBoxOperation.FormattingEnabled = true;
            this.comboBoxOperation.Items.AddRange(new object[] {
            "insert",
            "select",
            "update",
            "remove"});
            this.comboBoxOperation.Location = new System.Drawing.Point(21, 23);
            this.comboBoxOperation.Name = "comboBoxOperation";
            this.comboBoxOperation.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOperation.TabIndex = 2;
            this.comboBoxOperation.SelectedIndexChanged += new System.EventHandler(this.comboBoxOperation_SelectedIndexChanged);
            // 
            // studentsGridView
            // 
            this.studentsGridView.AllowUserToAddRows = false;
            this.studentsGridView.AllowUserToDeleteRows = false;
            this.studentsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsGridView.Location = new System.Drawing.Point(21, 116);
            this.studentsGridView.Name = "studentsGridView";
            this.studentsGridView.ReadOnly = true;
            this.studentsGridView.Size = new System.Drawing.Size(546, 240);
            this.studentsGridView.TabIndex = 1;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(584, 21);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(127, 23);
            this.buttonApply.TabIndex = 0;
            this.buttonApply.Text = "Apply";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonBackup
            // 
            this.buttonBackup.Location = new System.Drawing.Point(584, 304);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Size = new System.Drawing.Size(127, 23);
            this.buttonBackup.TabIndex = 11;
            this.buttonBackup.Text = "Backup";
            this.buttonBackup.UseVisualStyleBackColor = true;
            this.buttonBackup.Click += new System.EventHandler(this.buttonBackup_Click);
            // 
            // buttonRestore
            // 
            this.buttonRestore.Location = new System.Drawing.Point(584, 333);
            this.buttonRestore.Name = "buttonRestore";
            this.buttonRestore.Size = new System.Drawing.Size(127, 23);
            this.buttonRestore.TabIndex = 12;
            this.buttonRestore.Text = "Restore";
            this.buttonRestore.UseVisualStyleBackColor = true;
            this.buttonRestore.Click += new System.EventHandler(this.buttonRestore_Click);
            // 
            // DatabaseAppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "DatabaseAppForm";
            this.Text = "DatabaseApp";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxDatabaseName;
        private System.Windows.Forms.Button buttonCloseDatabase;
        private System.Windows.Forms.Button buttonFlushDatabase;
        private System.Windows.Forms.Button buttonOpenDatabase;
        private System.Windows.Forms.Button buttonDeleteDatabase;
        private System.Windows.Forms.Button buttonCreateDatabase;
        private System.Windows.Forms.Label labelLogs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelDatabases;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.DataGridView studentsGridView;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.ComboBox comboBoxOperation;
        private System.Windows.Forms.ComboBox comboBoxField;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.TextBox textBoxAverageScore;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Button buttonRestore;
        private System.Windows.Forms.Button buttonBackup;
    }
}

